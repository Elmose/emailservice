# README #

A simple email service has been developed as part of a coding challenge for an interview at Uber.

### Purpose ###

* Problem: An email service is required. The service should take some relevant input defining a mail, and send the mail. The service should provide an abstraction between two different email service providers.
If one service provider goes down, the service should quickly failover to a different provider without affecting the customers.
* Solution: A simple JSON REST API allows the user to define a mail which should be sent. The mail will be attempted sent by a number of configured mail service providers, until the mail is successfully sent, or alternatively all mail service providers fail in sending the mail, in which case the user will be notified with an error message.

### High level explanation of the code ###
* A client can access a single resource with an API key, and a definition of a mail to be sent
* In the datastore the following entities are defined:
 * MailServiceProvider: A mail service provider is defined by a base URL for the API, as well as the type of the mail service provider. This is used to map to the service implementation that contains the business logic for the given mail service provider.
 * Customer: A customer has a name and an API key for the Email Service API
 * CustomerProviderConfiguration: A customer provider configuration maps a customer with a mail service provider, and defines a number of parameters which can be different from customer to customer. Currently this is 1) what priority a given mail service provider has, i.e. in which failover order the mail service providers should be used, and 2) the API key the customer has for the given mail service provider. 
* When the Email Service API is called, the customer is deduced from the API key, and hence the customer provider configurations are fetched and used to attempt to send the mail to the configured mail service providers in the prioritized order.

### Technical choices and improvements ###
* Each DAO class accesses a corresponding entity class
* Business logic is to be held a the "service level", i.e. in the service classes, such that it is not spread to e.g. the DAO classes. Each service class has a corresponding DAO class - it is the responsibility of the service to "protect" the DAO and ensure that data as returned by the service is sound. If services need other entity data than the entity they access through their assigned DAO, the service must use another service, but should not access another DAO directly.
* Currently the user of the Email Service API must wait for the mail to be sent, before they get a response from the service. That is of course not acceptable as this could be a while or timeout while potentially many mail service providers are tried out. Instead the mail details should be persisted, and the id of the mail returned to the user in the synchronous call to the Email Service API, for future reference. The Email Service API should be extended with the possibility of querying the send status of a given mail id, or alternatively the user should be able to define an endpoint which the Email Service API can call when there is a final result for the email (i.e. sent or not sent after a given number of attempts.
* Currently the failover mechanism will attempt to use all defined email service providers for each mail in the priority order defined by the customer. If a given mail service provider is expected to be down for a long time, the failover mechanism could be made such that when it fails one or a given number of times, the system changes it priority such that it is moved back in the line of mail service providers. It depends on how important it is for the customer to use certain email providers instead of others (pricing etc.). 

### Focus area ###
The application is backend only.

### Suggestions for future work ###
* Make the Email Service scale by allowing for sending mails in bulk
* Make the Email Service more fault tolerant in relation to failures in the email service providers by allowing failed mails to be queued for one or more later send attempts
* Add integration tests
* Make the Email Service able to schedule when mails should be sent (and when to stop sending, if the mail has not succeeded before a given date and time)
* Enhance documentation by adding Swagger documentation on the Email Service
* Extend the supported content types of mails
* Extend the Email Service API, e.g. allowing for viewing status of previously scheduled mails or other statistics, and allowing the customers to change priority of the mail service providers
* Improve information for solving reported bugs by adding created and last modified data to datastore
* Add more unit tests

### Link to hosted application ###
-------------
The application is hosted on Google App Engine:
http://elmose-emailservice.appspot.com/api/v1/mail/send.json
Two mail service providers are configured - SendGrid and Mandrill. Note that both runs in test mode, i.e. no actual mails are sent.

### Accessing the Email Service API ###
Example:
For testing purposes, the API key to the email service is "testApiKey".
There are two configured mail service providers.
``` sh
curl -X POST \
  http://elmose-emailservice.appspot.com/api/v1/mail/send.json \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'x-email-service-api-key: testApiKey' \
  -d '{"fromMailAddress":{"mailAddress":"from@test.com"},"toMailAddress":{"mailAddress":"to@test.com"},"subject":"Some subject","content":"Some content"}'
```