package com.elmose.emailservice.model.entity;

import com.elmose.emailservice.api.v1.ApiKeyHandler;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;

/**
 * A model class representing a customer provider configuration of a mail service provider.
 * The configuration can change from customer to customer, e.g. each customer can prioritize the mail service provider differently, and each customer will have their own API keys for the mail service providers.
 */
@Entity
public class CustomerProviderConfiguration implements Serializable {
    @SuppressWarnings("unused")
    @Id
    private Long id;
    @Index
    private Ref<Customer> customerKey;
    private Ref<MailServiceProvider> mailServiceProviderKey;
    private String encodedProviderApiKey;
    private int priority;

    public Long getId() {
        return id;
    }

    public Ref<Customer> getCustomerKey() {
        return customerKey;
    }

    public void setCustomerKey(Customer customer) {
        this.customerKey = Ref.create(customer);
    }

    public Ref<MailServiceProvider> getMailServiceProviderKey() {
        return mailServiceProviderKey;
    }

    public void setMailServiceProviderKey(MailServiceProvider mailServiceProvider) {
        this.mailServiceProviderKey = Ref.create(mailServiceProvider);
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getDecodedApiKeyFromEncodedApiKey() {
        return ApiKeyHandler.getDecodedApiKeyFromEncodedApiKey(encodedProviderApiKey);
    }

    public void setEncodedApiKeyFromDecodedApiKey(String decodedApiKey) {
        this.encodedProviderApiKey = ApiKeyHandler.getEncodedApiKeyFromDecodedApiKey(decodedApiKey);
    }

    @Override
    public String toString() {
        return "CustomerProviderConfiguration{" +
                "id=" + id +
                ", customerKey=" + customerKey +
                ", mailServiceProviderKey=" + mailServiceProviderKey +
                ", priority=" + priority +
                '}';
    }
}
