package com.elmose.emailservice.model;

/**
 * The enum defines the different types of mail service providers.
 */
public enum MailServiceProviderType {
    SEND_GRID, MANDRILL
}
