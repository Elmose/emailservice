package com.elmose.emailservice.model.entity;

import com.elmose.emailservice.model.MailServiceProviderType;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.io.Serializable;

/**
 * A model class representing a mail service provider which can be used by the email service API.
 */
@Entity
public class MailServiceProvider implements Serializable {
    @Id
    private Long id;
    private String baseUrl;
    private MailServiceProviderType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public MailServiceProviderType getType() {
        return type;
    }

    public void setType(MailServiceProviderType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "MailServiceProvider{" +
                "id=" + id +
                ", baseUrl='" + baseUrl + '\'' +
                ", type=" + type +
                '}';
    }
}
