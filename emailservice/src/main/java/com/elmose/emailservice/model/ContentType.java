package com.elmose.emailservice.model;

public enum ContentType {
    PLAIN_TEXT("text/plain");

    private final String id;

    ContentType(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
