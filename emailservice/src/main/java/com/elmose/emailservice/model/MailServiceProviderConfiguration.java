package com.elmose.emailservice.model;

import com.elmose.emailservice.model.entity.CustomerProviderConfiguration;
import com.elmose.emailservice.model.entity.MailServiceProvider;

/**
 * A mail service provider configuration has all the details for a mail service provider.
 * The static details are defined in {@paramref mailServiceProvider}, and the dynamic details that might differ from customer to customer are defined in {@paramref customerProviderConfiguration}.
 */
public class MailServiceProviderConfiguration {
    private final MailServiceProvider mailServiceProvider;
    private final CustomerProviderConfiguration customerProviderConfiguration;

    public MailServiceProviderConfiguration(MailServiceProvider mailServiceProvider, CustomerProviderConfiguration customerProviderConfiguration) {
        this.mailServiceProvider = mailServiceProvider;
        this.customerProviderConfiguration = customerProviderConfiguration;
    }

    public MailServiceProvider getMailServiceProvider() {
        return mailServiceProvider;
    }

    public CustomerProviderConfiguration getCustomerProviderConfiguration() {
        return customerProviderConfiguration;
    }
}
