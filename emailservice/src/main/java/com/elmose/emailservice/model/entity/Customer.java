package com.elmose.emailservice.model.entity;

import com.elmose.emailservice.api.v1.ApiKeyHandler;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.io.Serializable;

/**
 * A model class representing a customer using the email service API.
 */
@Entity
public class Customer implements Serializable {
    @Id
    private Long id;
    private String name;
    @Index
    private String encodedMailServiceApiKey;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDecodedApiKeyFromEncodedApiKey() {
        return ApiKeyHandler.getDecodedApiKeyFromEncodedApiKey(encodedMailServiceApiKey);
    }

    public void setEncodedApiKeyFromDecodedApiKey(String decodedApiKey) {
        this.encodedMailServiceApiKey = ApiKeyHandler.getEncodedApiKeyFromDecodedApiKey(decodedApiKey);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
