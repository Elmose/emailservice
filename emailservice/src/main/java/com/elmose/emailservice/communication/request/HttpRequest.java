package com.elmose.emailservice.communication.request;

import com.elmose.emailservice.communication.response.Response;
import com.elmose.emailservice.exception.communication.RequestGenerationException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * A HTTP request for communication with external mail service provider APIs.
 */
public abstract class HttpRequest {
    private final Logger logger = Logger.getLogger(HttpRequest.class.getName());
    private final String baseEndpoint;
    private final String subUrl;
    private final Map<String, String> requestProperties;

    /**
     *
     * @param baseEndpoint The base endpoint of the external mail service provider API.
     * @param subUrl The sub url of the resource of the external mail service provider API.
     * @param requestProperties Header key-values, if any.
     */
    HttpRequest(String baseEndpoint, String subUrl, Map<String, String> requestProperties) {
        this.baseEndpoint = baseEndpoint;
        this.subUrl = subUrl;
        if (requestProperties == null){
            this.requestProperties = new HashMap<>();
        }else {
            this.requestProperties = requestProperties;
        }
    }

    /**
     * Executes a given request.
     * @return A response with the HTTP code as well as any returned content.
     */
    public Response execute() {
        OutputStream outputStream = null;
        try {
            HttpURLConnection connection = createConnection();
            if (getMessage().isPresent()) {
                logger.info("Sending message:\r\n" + getMessage());
                outputStream = connection.getOutputStream();
                outputStream.write(getMessage().get().getBytes("UTF-8"));
                outputStream.flush();
            }

            Response response = new Response(getResponseText(connection.getInputStream()), connection.getResponseCode());
            logger.info("Returned response is: " + response);
            connection.disconnect();
            return response;
        } catch (Exception e) {
            throw new RequestGenerationException(e);
        } finally {
            if (outputStream != null){
                try {
                    outputStream.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    /**
     * Converts the response from the external mail service provider API from a stream to a string.
     * @param inputStream The stream from the connection to the external API.
     * @return The content of the stream converted to a string.
     * @throws IOException If an I/O error occurs.
     */
    private String getResponseText(InputStream inputStream) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        String line = bufferedReader.readLine();
        while (line != null){
            stringBuffer.append(line);
            line = bufferedReader.readLine();
        }
        return stringBuffer.toString();
    }

    /**
     * @return A HTTP URL connection to an external mail service provider API.
     * @throws IOException If an I/O exception occurs.
     */
    private HttpURLConnection createConnection() throws IOException {
        String urlString = baseEndpoint + subUrl;
        logger.info("Creating " + getRequestMethod() + " connection to URL:\r\n" + urlString);
        URL url = new URL(urlString);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        if (getMessage().isPresent()) {
            connection.setDoOutput(true);
        }
        connection.setRequestMethod(getRequestMethod());
        connection.setConnectTimeout(6000);
        connection.setReadTimeout(5000);
        connection.setRequestProperty("Accept-Charset", "UTF-8");
        for (String key : requestProperties.keySet()) {
            connection.setRequestProperty(key, requestProperties.get(key));
        }
        return connection;
    }

    protected abstract String getRequestMethod();

    public abstract Optional<String> getMessage();
}