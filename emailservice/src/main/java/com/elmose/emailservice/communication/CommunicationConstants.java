package com.elmose.emailservice.communication;

/**
 * Constants used in the process of communication with an external mail service provider API
 */
public interface CommunicationConstants {
    String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
    String HTTP_HEADER_VALUE_APPLICATION_JSON = "application/json";
}
