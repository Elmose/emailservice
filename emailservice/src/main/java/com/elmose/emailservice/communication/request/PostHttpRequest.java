package com.elmose.emailservice.communication.request;

import java.util.Map;
import java.util.Optional;

/**
 * A POST HTTP request, see {@link HttpRequest}
 */
public class PostHttpRequest extends HttpRequest {
    private final String message;

    public PostHttpRequest(String baseEndpoint, String subUrl, String message) {
        super(baseEndpoint, subUrl, null);
        this.message = message;
    }

    public PostHttpRequest(String baseEndpoint, String subUrl, Map<String, String> requestProperties, String message) {
        super(baseEndpoint, subUrl, requestProperties);
        this.message = message;
    }

    @Override
    public String getRequestMethod() {
        return "POST";
    }

    @Override
    public Optional<String> getMessage() {
        return Optional.ofNullable(message);
    }
}