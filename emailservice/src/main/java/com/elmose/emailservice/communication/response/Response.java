package com.elmose.emailservice.communication.response;

/**
 * The class represents the result of a call to an external mail service provider API with a HTTP status code and a response text
 */
public class Response {
    private final String responseText;
    private final int httpStatusCode;

    public Response(String responseText, int httpStatusCode) {
        this.responseText = responseText;
        this.httpStatusCode = httpStatusCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    @Override
    public String toString() {
        return "Response{" +
                "responseText='" + responseText + '\'' +
                ", httpStatusCode=" + httpStatusCode +
                '}';
    }
}
