package com.elmose.emailservice.service;

import com.elmose.emailservice.dao.MailServiceProviderDao;
import com.elmose.emailservice.model.entity.MailServiceProvider;

import java.util.List;

/**
 * A mail service provider service accessing the related DAO. The service contains all relevant business logic.
 */
class MailServiceProviderService {
    private final MailServiceProviderDao dao = new MailServiceProviderDao();

    public List<MailServiceProvider> getMailServiceProviders(){
        return dao.getAll();
    }
}
