package com.elmose.emailservice.service;

import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.mailserviceprovider.service.MailSenderServicesFailoverHandler;
import com.elmose.emailservice.model.MailServiceProviderConfiguration;
import com.elmose.emailservice.model.entity.Customer;
import com.elmose.emailservice.model.entity.CustomerProviderConfiguration;
import com.elmose.emailservice.model.entity.MailServiceProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * A mail service. The service contains all relevant business logic for mails.
 */
public class MailService {
    private static final Logger LOGGER = Logger.getLogger(MailService.class.getName());

    public boolean sendMail(String apiKey, MailMessage mailMessage) {
        List<MailServiceProviderConfiguration> mailServiceProviderConfigurations = getMailServiceProviderConfigurations(apiKey);

        MailSenderServicesFailoverHandler mailSenderServicesFailoverHandler = new MailSenderServicesFailoverHandler(mailServiceProviderConfigurations);
        boolean mailSent = false;
        while (!mailSent){
            mailSent = mailSenderServicesFailoverHandler.sendMail(mailMessage);
            if (!mailSent){
                LOGGER.warning("Mail " + mailMessage + " was not sent");
            }
        }
        return mailSent;
    }

    private List<MailServiceProviderConfiguration> getMailServiceProviderConfigurations(String apiKey) {
        Customer customer = new CustomerService().getCustomerByDecodedApiKey(apiKey);
        List<MailServiceProvider> mailServiceProviders = new MailServiceProviderService().getMailServiceProviders();
        List<CustomerProviderConfiguration> customerProviderConfigurations = new CustomerProviderConfigurationService().getByCustomer(customer);
        List<MailServiceProviderConfiguration> providerConfigurations = new ArrayList<>();
        for (CustomerProviderConfiguration customerProviderConfiguration : customerProviderConfigurations) {
            for (MailServiceProvider mailServiceProvider : mailServiceProviders) {
                if (customerProviderConfiguration.getMailServiceProviderKey().get().getId().equals(mailServiceProvider.getId())){
                    providerConfigurations.add(new MailServiceProviderConfiguration(mailServiceProvider, customerProviderConfiguration));
                }
            }
        }
        return providerConfigurations;
    }
}
