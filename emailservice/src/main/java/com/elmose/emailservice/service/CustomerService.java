package com.elmose.emailservice.service;

import com.elmose.emailservice.api.v1.ApiKeyHandler;
import com.elmose.emailservice.dao.CustomerDao;
import com.elmose.emailservice.exception.UnknownEmailServiceApiKeyException;
import com.elmose.emailservice.model.entity.Customer;

import java.util.Optional;

/**
 * A customer service accessing the related DAO. The service contains all relevant business logic
 */
class CustomerService {
    private final CustomerDao dao = new CustomerDao();

    public Customer getCustomerByDecodedApiKey(String decodedApiKey){
        Optional<Customer> customer = dao.getByEncodedApiKey(ApiKeyHandler.getEncodedApiKeyFromDecodedApiKey(decodedApiKey));
        if (!customer.isPresent()){
            throw new UnknownEmailServiceApiKeyException();
        }
        return customer.get();
    }
}
