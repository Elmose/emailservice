package com.elmose.emailservice.service;

import com.elmose.emailservice.dao.CustomerProviderConfigurationDao;
import com.elmose.emailservice.model.entity.Customer;
import com.elmose.emailservice.model.entity.CustomerProviderConfiguration;
import com.googlecode.objectify.Ref;

import java.util.List;

/**
 * A customer provider configuration service accessing the related DAO. The service contains all relevant business logic
 */
class CustomerProviderConfigurationService {
    private final CustomerProviderConfigurationDao dao = new CustomerProviderConfigurationDao();

    public List<CustomerProviderConfiguration> getByCustomer(Customer customer){
        return dao.getByCustomerKey(Ref.create(customer));
    }
}
