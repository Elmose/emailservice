package com.elmose.emailservice.dao;

import com.elmose.emailservice.model.entity.Customer;
import com.elmose.emailservice.model.entity.CustomerProviderConfiguration;
import com.elmose.emailservice.model.entity.MailServiceProvider;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * A context listener registering all the entities in the datastore within the Objectify framework
 */
public class InitializerContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {
        ObjectifyFactory factory = ObjectifyService.factory();
        factory.register(Customer.class);
        factory.register(MailServiceProvider.class);
        factory.register(CustomerProviderConfiguration.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
