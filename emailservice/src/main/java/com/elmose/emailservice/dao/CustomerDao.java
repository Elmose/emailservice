package com.elmose.emailservice.dao;

import com.elmose.emailservice.model.entity.Customer;

import java.util.List;
import java.util.Optional;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * A data access object for the {@link Customer} model.
 */
public class CustomerDao {
    /**
     * Creates or updates a customer
     * @param customer The customer to create or update
     */
    public void createOrUpdate(final Customer customer){
        ofy().save().entity(customer).now();
    }

    /**
     * @param encodedApiKey An encoded version of an API key from which the customer should be selected.
     * @return A customer, if any, which matches the given API key.
     */
    public Optional<Customer> getByEncodedApiKey(final String encodedApiKey) {
        return Optional.ofNullable(ofy().load().type(Customer.class).filter("encodedMailServiceApiKey", encodedApiKey).first().now());
    }

    /**
     * @return All customers in the datastore.
     */
    public List<Customer> getAll() {
        return ofy().load().type(Customer.class).list();
    }
}
