package com.elmose.emailservice.dao;

import com.elmose.emailservice.model.entity.MailServiceProvider;

import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * A data access object for the {@link MailServiceProvider} model.
 */
public class MailServiceProviderDao {
    /**
     * Creates or updates a mail service provider.
     * @param mailServiceProvider The mail service provider to create or update.
     */
    public void createOrUpdate(final MailServiceProvider mailServiceProvider){
        ofy().save().entity(mailServiceProvider).now();
    }

    /**
     * @return All mail service providers in the datastore.
     */
    public List<MailServiceProvider> getAll() {
        return ofy().load().type(MailServiceProvider.class).list();
    }
}
