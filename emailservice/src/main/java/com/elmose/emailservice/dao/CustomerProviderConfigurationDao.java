package com.elmose.emailservice.dao;

import com.elmose.emailservice.model.entity.Customer;
import com.elmose.emailservice.model.entity.CustomerProviderConfiguration;
import com.googlecode.objectify.Ref;

import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * A data access object for the {@link CustomerProviderConfiguration} model.
 */
public class CustomerProviderConfigurationDao {
    /**
     * Creates or updates a customer provider configuration.
     * @param customerProviderConfiguration The customer provider configuration to create or update.
     */
    public void createOrUpdate(final CustomerProviderConfiguration customerProviderConfiguration){
        ofy().save().entity(customerProviderConfiguration).now();
    }

    /**
     * @param customerKey A key for a customer.
     * @return A list of customer provider configurations, if any, which matches the given customer.
     */
    public List<CustomerProviderConfiguration> getByCustomerKey(Ref<Customer> customerKey) {
        return ofy().load().type(CustomerProviderConfiguration.class).filter("customerKey", customerKey).list();
    }

    /**
     * @return All customer provider configurations in the datastore.
     */
    public List<CustomerProviderConfiguration> getAll() {
        return ofy().load().type(CustomerProviderConfiguration.class).list();
    }
}
