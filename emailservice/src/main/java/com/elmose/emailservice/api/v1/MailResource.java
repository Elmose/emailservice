package com.elmose.emailservice.api.v1;

import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.api.v1.model.MailResult;
import com.elmose.emailservice.service.MailService;

import javax.validation.*;
import javax.ws.rs.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;
import java.util.logging.Logger;

/**
 * A mail resource in the REST API with which mail messages can be sent via a mail service provider, with a set of mail service providers as possible failovers.
 */
@Path("/mail")
public class MailResource {
    private static final Logger LOGGER = Logger.getLogger(MailResource.class.getName());
    private static final String API_KEY_HEADER = "x-email-service-api-key";

    /**
     * Sends the given email via a mail service provider. If one or more service providers fails to send the mail, the mail will be attempted sent by other configured mail service providers, if any.
     * @param apiKey An API key giving access to use the email service API
     * @param mailMessage The mail message to send
     * @return Whether the mail was sent or not
     */
    @Path("/send.json")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response send(@HeaderParam(API_KEY_HEADER) String apiKey, MailMessage mailMessage){
        LOGGER.info("Send request received for mail message " + mailMessage);

        validateInput(apiKey, mailMessage);

        Set<ConstraintViolation<MailMessage>> violations = getViolations(mailMessage);
        if (violations.isEmpty()){
            boolean mailSent = new MailService().sendMail(apiKey, mailMessage);
            return Response.ok(new MailResult(mailSent)).build();
        }else{
            throw new ConstraintViolationException(violations);
        }
    }

    /**
     * The method validates the input parameters to the REST resource.
     * Preferable the javax.validation framework should be used instead.
     * @param apiKey The HTTP header API key.
     * @param mailMessage A mail message defining the content of the mail as well as the sender and the receiver.
     */
    private void validateInput(String apiKey, MailMessage mailMessage) {
        if (apiKey == null || apiKey.trim().equals("")){
            throw new ConstraintViolationException("Api key is not defined in HTTP header " + API_KEY_HEADER, null);
        }
        if (mailMessage == null){
            throw new ConstraintViolationException("The mail message is not defined", null);
        }
    }

    /**
     * Validation of the mail message via the javax.validation framework.
     * @param mailMessage The mail message to validate.
     * @return A set of violated constraints on the mail message, if any.
     */
    private Set<ConstraintViolation<MailMessage>> getViolations(MailMessage mailMessage) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        return validator.validate(mailMessage);
    }
}
