package com.elmose.emailservice.api.v1.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * The class represents the result of an attempt to send a mail, i.e. denoting whether the mail was sent or not.
 */
@XmlRootElement
public class MailResult {
    private boolean mailSent;

    @SuppressWarnings("unused")
    private MailResult() {
    }

    public MailResult(boolean mailSent) {
        this.mailSent = mailSent;
    }

    @SuppressWarnings("unused")
    public boolean isMailSent() {
        return mailSent;
    }

    @SuppressWarnings("unused")
    public void setMailSent(boolean mailSent) {
        this.mailSent = mailSent;
    }
}
