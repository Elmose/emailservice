package com.elmose.emailservice.api.v1.exceptionmapper;

import com.elmose.emailservice.api.v1.model.ErrorResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An exception mapper for {@link Throwable}.
 * The mapper converts a ConstraintViolationException to an error response {@link ErrorResponse} returned by the email service API.
 */
@Provider
public class GeneralExceptionMapper implements ExceptionMapper<Throwable>{
    private static final Logger LOGGER = Logger.getLogger(GeneralExceptionMapper.class.getName());
    private static final String ERROR_MESSAGE = "An unexpected server error occurred";

    @Override
    public Response toResponse(Throwable throwable) {
        LOGGER.log(Level.WARNING, ERROR_MESSAGE, throwable);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                .entity(new ErrorResponse(10000, ERROR_MESSAGE))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}
