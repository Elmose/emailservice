package com.elmose.emailservice.api.v1.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * The class represents an error response in the email service API with an error code and an error message.
 */
@XmlRootElement
public class ErrorResponse {
    private int errorCode;
    private String errorMessage;

    @SuppressWarnings("unused")
    private ErrorResponse() {
    }

    public ErrorResponse(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    @SuppressWarnings("unused")
    public int getErrorCode() {
        return errorCode;
    }

    @SuppressWarnings("unused")
    public String getErrorMessage() {
        return errorMessage;
    }

    @SuppressWarnings("unused")
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    @SuppressWarnings("unused")
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
