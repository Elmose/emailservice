package com.elmose.emailservice.api.v1.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A model representing a simple mail message
 */
@XmlRootElement
public class MailMessage {
    @Valid
    @NotNull
    private MailAddress fromMailAddress;
    @Valid
    @NotNull
    private MailAddress toMailAddress;
    @NotNull
    private String subject;
    @NotNull
    private String content;

    @SuppressWarnings("unused")
    private MailMessage() {
    }

    public MailMessage(MailAddress fromMailAddress, MailAddress toMailAddress, String subject, String content) {
        this.toMailAddress = toMailAddress;
        this.fromMailAddress = fromMailAddress;
        this.subject = subject;
        this.content = content;
    }

    public MailAddress getToMailAddress() {
        return toMailAddress;
    }

    public MailAddress getFromMailAddress() {
        return fromMailAddress;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    @SuppressWarnings("unused")
    public void setFromMailAddress(MailAddress fromMailAddress) {
        this.fromMailAddress = fromMailAddress;
    }

    @SuppressWarnings("unused")
    public void setToMailAddress(MailAddress toMailAddress) {
        this.toMailAddress = toMailAddress;
    }

    @SuppressWarnings("unused")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @SuppressWarnings("unused")
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "MailMessage{" +
                "fromMailAddress=" + fromMailAddress +
                ", toMailAddress=" + toMailAddress +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
