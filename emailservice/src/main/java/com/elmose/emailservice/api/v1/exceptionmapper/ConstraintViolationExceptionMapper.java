package com.elmose.emailservice.api.v1.exceptionmapper;

import com.elmose.emailservice.api.v1.model.ErrorResponse;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * An exception mapper for ConstraintViolationException.
 * The mapper converts a ConstraintViolationException to an error response {@link ErrorResponse} returned by the email service API.
 */
@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException>{
    private static final String ERROR_MESSAGE_PREFIX = "One or more requirements were violated: ";

    @Override
    public Response toResponse(ConstraintViolationException violationException) {
        return Response.status(Response.Status.BAD_REQUEST.getStatusCode())
                .entity(new ErrorResponse(3000, getErrorMessage(violationException)))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

    private String getErrorMessage(ConstraintViolationException violationException) {
        return ERROR_MESSAGE_PREFIX + violationException.getMessage();
    }
}
