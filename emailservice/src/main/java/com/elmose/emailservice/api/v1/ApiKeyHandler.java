package com.elmose.emailservice.api.v1;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * The class can Base 64 encode and decode an API key with the purpose of not having the API key in cleartext in the datastore
 */
public class ApiKeyHandler {
    public static String getEncodedApiKeyFromDecodedApiKey(String decodedApiKey) {
        try {
            return Base64.getEncoder().encodeToString(decodedApiKey.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static String getDecodedApiKeyFromEncodedApiKey(String encodedProviderApiKey) {
        byte[] decoded = Base64.getDecoder().decode(encodedProviderApiKey);
        try {
            return new String(decoded, "utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
