package com.elmose.emailservice.api.v1.model;

import com.elmose.emailservice.exception.mailaddress.AbsentMailAddressException;
import com.elmose.emailservice.exception.mailaddress.InvalidMailAddressException;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * The class represents an email address.
 * An email address will be validated in relation to RFC 5322
 */
@XmlRootElement
public class MailAddress {
    @NotNull
    private String mailAddress;
    private static final String EMAIL_REGEX_RFC_5322 = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    @SuppressWarnings("unused")
    private MailAddress() {
    }

    public MailAddress(String mailAddress) {
        setMailAddress(mailAddress);
    }

    private void validateEmailAddress(String emailAddress) {
        if (emailAddress.trim().equals("")){
            throw new AbsentMailAddressException();
        }
        if (!Pattern.matches(EMAIL_REGEX_RFC_5322, emailAddress)){
            throw new InvalidMailAddressException("The email address " + emailAddress + " is not valid");
        }
    }

    public String getMailAddress() {
        return mailAddress;
    }

    private void setMailAddress(String mailAddress) {
        validateEmailAddress(mailAddress);
        this.mailAddress = mailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailAddress that = (MailAddress) o;
        return Objects.equals(mailAddress, that.mailAddress);
    }

    /**
     * @return A string representation of the class, anonymizing half of the email address in case the data is e.g. logged
     */
    @Override
    public String toString() {
        return "MailAddress{" +
                "mailAddress='" + mailAddress.substring(0,mailAddress.length()/2) + "***\'" +
                '}';
    }
}
