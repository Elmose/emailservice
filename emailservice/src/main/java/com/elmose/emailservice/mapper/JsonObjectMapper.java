package com.elmose.emailservice.mapper;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.map.type.TypeFactory;

import java.io.IOException;
import java.util.List;

/**
 * A utility class for mapping objects to JSON and vice versa.
 * @param <T>
 */
public class JsonObjectMapper<T> {
    public String toJsonString(T object){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(object);
        } catch (IOException e) {
            throw new RuntimeException("Could not convert object " + object + " to JSON", e);
        }
    }

    public List<T> toObjectListIgnoreUnknownProperties(String jsonString, Class clazz){
        ObjectMapper mapper = new ObjectMapper();
        TypeFactory typeFactory = mapper.getTypeFactory();
        CollectionType collectionType = typeFactory.constructCollectionType(List.class, clazz);
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            return mapper.readValue(jsonString, collectionType);
        } catch (IOException e) {
            throw new RuntimeException("Could not convert json string to object of type " + clazz, e);
        }
    }
}
