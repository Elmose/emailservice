package com.elmose.emailservice.exception;

/**
 * The exception is thrown if the API key for the email service defined by the user is not defined in the datastore
 */
public class UnknownEmailServiceApiKeyException extends BusinessException {
    public UnknownEmailServiceApiKeyException() {
        super(4000, "The provided API key is unknown");
    }
}
