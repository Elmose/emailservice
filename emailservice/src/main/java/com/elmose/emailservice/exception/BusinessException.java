package com.elmose.emailservice.exception;

import com.elmose.emailservice.api.v1.model.ErrorResponse;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.HttpURLConnection;

/**
 * All business exceptions are will get a HTTP code 400, and return a {@link ErrorResponse} to the user of the API.
 */
public abstract class BusinessException extends WebApplicationException {
    protected BusinessException(int errorCode, String errorMessage) {
        super(Response.status(HttpURLConnection.HTTP_BAD_REQUEST).entity(new ErrorResponse(errorCode, errorMessage)).type(MediaType.APPLICATION_JSON).build());
    }
}
