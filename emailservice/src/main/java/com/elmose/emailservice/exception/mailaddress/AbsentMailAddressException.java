package com.elmose.emailservice.exception.mailaddress;

import com.elmose.emailservice.exception.BusinessException;

/**
 * The exception is thrown if a mandatory email address has not been defined by the user
 */
public class AbsentMailAddressException extends BusinessException {
    public AbsentMailAddressException() {
        super(1000, "The email address has not been defined");
    }
}
