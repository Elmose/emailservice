package com.elmose.emailservice.exception.mailsender;

import com.elmose.emailservice.exception.BusinessException;

/**
 * The exception is thrown if a mail has been attempted sent by all configured mail service providers, and non of them were successful in sending the mail.
 */
public class NoMoreMailSendersException extends BusinessException {
    public NoMoreMailSendersException() {
        super(2001, "No mail senders could successfully send the email");
    }
}
