package com.elmose.emailservice.exception.mailaddress;

import com.elmose.emailservice.exception.BusinessException;

/**
 * The exception is thrown if an email address defined by the user is not valid
 */
public class InvalidMailAddressException extends BusinessException {
    public InvalidMailAddressException(String errorMessage) {
        super(1001, errorMessage);
    }
}
