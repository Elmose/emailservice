package com.elmose.emailservice.exception.communication;

/**
 * The exception is thrown if e.g. an I/O exception occurs in relation to creating a HTTP connection to the external mail service provider API
 */
public class RequestGenerationException extends RuntimeException {
    public RequestGenerationException(Exception e) {
        super(e);
    }
}
