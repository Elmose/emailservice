package com.elmose.emailservice.exception.mailsender;

import com.elmose.emailservice.exception.BusinessException;

/**
 * The exception is thrown if there exists no customer configuration for a given mail service provider.
 */
public class MissingMailServiceProviderConfigurationException extends BusinessException{
    public MissingMailServiceProviderConfigurationException(String errorMessage) {
        super(2000, errorMessage);
    }
}
