package com.elmose.emailservice.mailserviceprovider.sendgrid.model;

import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.model.ContentType;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.Objects;

/**
 * The model class represents mail content when a request has been made to send a mail in relation to the SendGrid mail service API.
 */
@JsonPropertyOrder(alphabetic=true)
class SendGridContent {
    private String type;
    private String value;

    @SuppressWarnings("unused")
    private SendGridContent() {
    }

    private SendGridContent(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public SendGridContent(MailMessage mailMessage) {
        this(ContentType.PLAIN_TEXT.getId(), mailMessage.getContent());
    }

    @SuppressWarnings("unused")
    public String getType() {
        return type;
    }

    @SuppressWarnings("unused")
    public void setType(String type) {
        this.type = type;
    }

    @SuppressWarnings("unused")
    public String getValue() {
        return value;
    }

    @SuppressWarnings("unused")
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendGridContent sendGridContent = (SendGridContent) o;
        return Objects.equals(type, sendGridContent.type) &&
                Objects.equals(value, sendGridContent.value);
    }
}
