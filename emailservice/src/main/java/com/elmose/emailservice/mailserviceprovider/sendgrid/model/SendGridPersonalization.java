package com.elmose.emailservice.mailserviceprovider.sendgrid.model;

import com.elmose.emailservice.api.v1.model.MailMessage;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The model class represents which mail addresses a given mail should be sent to, when a request has been made to send a mail in relation to the SendGrid mail service API.
 */
@JsonPropertyOrder(alphabetic=true)
class SendGridPersonalization {
    private List<SendGridMailAddress> to = new ArrayList<>();
    private String subject;

    @SuppressWarnings("unused")
    private SendGridPersonalization() {
    }

    public SendGridPersonalization(MailMessage mailMessage) {
        this(new SendGridMailAddress(mailMessage.getToMailAddress()), mailMessage.getSubject());
    }

    private SendGridPersonalization(SendGridMailAddress toMailAddress, String subject) {
        this.to.add(toMailAddress);
        this.subject = subject;
    }

    @SuppressWarnings("unused")
    public void setTo(List<SendGridMailAddress> to) {
        this.to = to;
    }

    @SuppressWarnings("unused")
    public String getSubject() {
        return subject;
    }

    @SuppressWarnings("unused")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<SendGridMailAddress> getTo() {
        return to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendGridPersonalization that = (SendGridPersonalization) o;
        return Objects.equals(to, that.to) &&
                Objects.equals(subject, that.subject);
    }
}
