package com.elmose.emailservice.mailserviceprovider.mandrill.model;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * The model class represents a mail response returned by the Mandrill mail service API, when a request has been made to send a mail.
 */
@JsonPropertyOrder(alphabetic=true)
public class MandrillMailResponse {
    private String status;
    @JsonProperty("reject_reason")
    private String rejectReason;
    public static final String MAIL_SENT_STATUS = "sent";

    @SuppressWarnings("unused")
    private MandrillMailResponse() {
    }

    public String getStatus() {
        return status;
    }

    @SuppressWarnings("unused")
    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    @SuppressWarnings("unused")
    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }
}
