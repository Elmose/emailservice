package com.elmose.emailservice.mailserviceprovider.sendgrid.model;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * The model class represents mail settings, currently limited to sandbox mode.
 */
@JsonPropertyOrder(alphabetic=true)
public class SendGridMailSettings {
    @JsonProperty("sandbox_mode")
    private SendGridSandboxMode sandboxMode;

    @SuppressWarnings("unused")
    private SendGridMailSettings() {
    }

    public SendGridMailSettings(SendGridSandboxMode sandboxMode) {
        this.sandboxMode = sandboxMode;
    }

    @SuppressWarnings("unused")
    public SendGridSandboxMode getSandboxMode() {
        return sandboxMode;
    }

    @SuppressWarnings("unused")
    public void setSandboxMode(SendGridSandboxMode sandboxMode) {
        this.sandboxMode = sandboxMode;
    }
}
