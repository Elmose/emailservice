package com.elmose.emailservice.mailserviceprovider.mandrill.model;

import com.elmose.emailservice.api.v1.model.MailMessage;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * The model class represents a mail message when a request has been made to send a mail in relation to the Mandrill mail service API.
 */
@JsonPropertyOrder(alphabetic=true)
public class MandrillMessage {
    private String html;
    private String subject;
    @JsonProperty("from_email")
    private String fromEmail;
    private List<MandrillMailAddress> to;

    @SuppressWarnings("unused")
    private MandrillMessage() {
    }

    public MandrillMessage(MailMessage mailMessage) {
        this.html = mailMessage.getContent();
        this.subject = mailMessage.getSubject();
        this.fromEmail = mailMessage.getFromMailAddress().getMailAddress();
        this.to = new ArrayList<>();
        to.add(new MandrillMailAddress(mailMessage.getToMailAddress()));
    }

    @SuppressWarnings("unused")

    public String getHtml() {
        return html;
    }

    @SuppressWarnings("unused")
    public void setHtml(String html) {
        this.html = html;
    }

    @SuppressWarnings("unused")
    public String getSubject() {
        return subject;
    }

    @SuppressWarnings("unused")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @SuppressWarnings("unused")
    public String getFromEmail() {
        return fromEmail;
    }

    @SuppressWarnings("unused")
    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    @SuppressWarnings("unused")
    public List<MandrillMailAddress> getTo() {
        return to;
    }

    @SuppressWarnings("unused")
    public void setTo(List<MandrillMailAddress> to) {
        this.to = to;
    }
}
