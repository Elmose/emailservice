package com.elmose.emailservice.mailserviceprovider.service;

import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.exception.mailsender.NoMoreMailSendersException;
import com.elmose.emailservice.mailserviceprovider.mandrill.service.MandrillMailService;
import com.elmose.emailservice.mailserviceprovider.sendgrid.service.SendGridMailService;
import com.elmose.emailservice.model.MailServiceProviderConfiguration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class MailSenderServicesFailoverHandler {
    private static final Logger LOGGER = Logger.getLogger(MailSenderServicesFailoverHandler.class.getName());
    private final List<MailSenderService> possibleMailSenderServices = new ArrayList<>();
    private Optional<MailSenderService> nextMailSender;

    public MailSenderServicesFailoverHandler(List<MailServiceProviderConfiguration> providerConfigurations) {
        List<MailSenderService> mailSenderServices = new ArrayList<>();
        mailSenderServices.add(new SendGridMailService(providerConfigurations));
        mailSenderServices.add(new MandrillMailService(providerConfigurations));
        setupMailSenders(mailSenderServices);
    }

    MailSenderServicesFailoverHandler(boolean ignore, List<MailSenderService> mailSenderServices) {
        setupMailSenders(mailSenderServices);
    }

    private void setupMailSenders(List<MailSenderService> mailSenderServices) {
        possibleMailSenderServices.addAll(mailSenderServices);
        Collections.sort(possibleMailSenderServices, (o1, o2) -> o1.getPriority() - o2.getPriority());
        setNextMailSender();
    }

    Optional<MailSenderService> getNextMailSender() {
        return nextMailSender;
    }

    private void setNextMailSender() {
        if (possibleMailSenderServices.isEmpty()){
            LOGGER.warning("No more available mail senders");
            nextMailSender = Optional.empty();
        }else{
            nextMailSender = Optional.ofNullable(possibleMailSenderServices.get(0));
            possibleMailSenderServices.remove(0);
            LOGGER.warning("Next mail sender is " + nextMailSender);
        }
    }

    public boolean sendMail(MailMessage mailMessage) {
        if (nextMailSender.isPresent()){
            boolean mailSent = nextMailSender.get().sendMail(mailMessage);
            LOGGER.info("Mail was " + (mailSent ? "successfully" : "not") + " sent");
            if (!mailSent){
                setNextMailSender();
            }
            return mailSent;
        }else{
            throw new NoMoreMailSendersException();
        }
    }
}
