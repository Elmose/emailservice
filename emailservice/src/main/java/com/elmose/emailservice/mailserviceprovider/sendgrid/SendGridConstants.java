package com.elmose.emailservice.mailserviceprovider.sendgrid;

/**
 * Constants related to the Mandrill API.
 */
public interface SendGridConstants {
    String AUTHORIZATION_HEADER_KEY = "Authorization";
    String AUTHORIZATION_HEADER_VALUE_PREFIX = "Bearer";

    String MAIL_SEND_SUBURL = "mail/send";
}
