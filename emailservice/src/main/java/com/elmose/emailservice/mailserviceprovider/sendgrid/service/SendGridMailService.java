package com.elmose.emailservice.mailserviceprovider.sendgrid.service;

import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.communication.CommunicationConstants;
import com.elmose.emailservice.communication.response.Response;
import com.elmose.emailservice.mailserviceprovider.sendgrid.SendGridConstants;
import com.elmose.emailservice.mailserviceprovider.sendgrid.api.SendGridClient;
import com.elmose.emailservice.mailserviceprovider.sendgrid.model.SendGridMailMessage;
import com.elmose.emailservice.mailserviceprovider.service.MailSenderService;
import com.elmose.emailservice.mapper.JsonObjectMapper;
import com.elmose.emailservice.model.MailServiceProviderConfiguration;
import com.elmose.emailservice.model.MailServiceProviderType;
import com.elmose.emailservice.model.entity.MailServiceProvider;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * The service sends a mail to the SendGrid mail service provider via the {@link SendGridClient} and interprets the result as to whether a mail was successfully sent.
 */
public class SendGridMailService extends MailSenderService<SendGridClient> {
    private static final Logger LOGGER = Logger.getLogger(SendGridMailService.class.getName());
    private static final MailServiceProviderType MAIL_SERVICE_PROVIDER_TYPE = MailServiceProviderType.SEND_GRID;

    public SendGridMailService(List<MailServiceProviderConfiguration> providerConfigurations) {
        super(providerConfigurations);
    }

    @Override
    public boolean sendMail(MailMessage mailMessage) {
        String message = getMessage(mailMessage);
        LOGGER.info("Sending mail " + message + " via SendGrid");
        Map<String, String> requestProperties = createRequestProperties();
        Response response = client.sendMail(requestProperties, message);
        return response.getHttpStatusCode() == HttpURLConnection.HTTP_ACCEPTED;
    }

    private String getMessage(MailMessage mailMessage) {
        return new JsonObjectMapper<SendGridMailMessage>().toJsonString(new SendGridMailMessage(mailMessage));
    }

    private Map<String, String> createRequestProperties() {
        Map<String, String> requestProperties = new HashMap<>();
        requestProperties.put(CommunicationConstants.HTTP_HEADER_CONTENT_TYPE, CommunicationConstants.HTTP_HEADER_VALUE_APPLICATION_JSON);
        requestProperties.put(SendGridConstants.AUTHORIZATION_HEADER_KEY, SendGridConstants.AUTHORIZATION_HEADER_VALUE_PREFIX + " " + mailServiceProviderConfiguration.getCustomerProviderConfiguration().getDecodedApiKeyFromEncodedApiKey());
        return requestProperties;
    }

    @Override
    protected SendGridClient createApiClient(MailServiceProvider mailServiceProvider) {
        return new SendGridClient(mailServiceProvider);
    }

    @Override
    protected MailServiceProviderType getMailServiceProviderType() {
        return MAIL_SERVICE_PROVIDER_TYPE;
    }
}
