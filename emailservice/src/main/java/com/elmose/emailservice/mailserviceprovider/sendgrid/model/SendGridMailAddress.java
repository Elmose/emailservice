package com.elmose.emailservice.mailserviceprovider.sendgrid.model;

import com.elmose.emailservice.api.v1.model.MailAddress;
import com.elmose.emailservice.api.v1.model.MailMessage;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.Objects;

/**
 * The model class represents a mail address content when a request has been made to send a mail in relation to the SendGrid mail service API.
 */
@JsonPropertyOrder(alphabetic=true)
public class SendGridMailAddress {
    private String email;

    @SuppressWarnings("unused")
    private SendGridMailAddress() {
    }

    public SendGridMailAddress(MailAddress email) {
        this.email = email.getMailAddress();
    }

    public SendGridMailAddress(MailMessage mailMessage) {
        this(mailMessage.getFromMailAddress());
    }

    @SuppressWarnings("unused")
    public String getEmail() {
        return email;
    }

    @SuppressWarnings("unused")
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendGridMailAddress that = (SendGridMailAddress) o;
        return Objects.equals(email, that.email);
    }
}
