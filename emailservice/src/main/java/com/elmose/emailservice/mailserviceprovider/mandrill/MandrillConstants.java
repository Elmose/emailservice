package com.elmose.emailservice.mailserviceprovider.mandrill;

/**
 * Constants related to the Mandrill API.
 */
public interface MandrillConstants {
    String MAIL_SEND_SUBURL = "messages/send.json";
}
