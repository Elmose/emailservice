package com.elmose.emailservice.mailserviceprovider.mandrill.model;

import com.elmose.emailservice.api.v1.model.MailMessage;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * The model class represents the entire mail message when a request has been made to send a mail in relation to the Mandrill mail service API.
 */
@JsonPropertyOrder(alphabetic=true)
public class MandrillMailMessage {
    private String key;
    private MandrillMessage message;

    @SuppressWarnings("unused")
    private MandrillMailMessage() {
    }

    public MandrillMailMessage(MailMessage mailMessage, String apiKey) {
        this.key = apiKey;
        this.message = new MandrillMessage(mailMessage);
    }

    @SuppressWarnings("unused")
    public String getKey() {
        return key;
    }

    @SuppressWarnings("unused")
    public void setKey(String key) {
        this.key = key;
    }

    @SuppressWarnings("unused")
    public MandrillMessage getMessage() {
        return message;
    }

    @SuppressWarnings("unused")
    public void setMessage(MandrillMessage message) {
        this.message = message;
    }
}
