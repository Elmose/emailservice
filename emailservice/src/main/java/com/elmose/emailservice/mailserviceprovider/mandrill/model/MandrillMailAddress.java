package com.elmose.emailservice.mailserviceprovider.mandrill.model;

import com.elmose.emailservice.api.v1.model.MailAddress;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * The model class represents a mail address when a request has been made to send a mail in relation to the Mandrill mail service API.
 */
@JsonPropertyOrder(alphabetic=true)
class MandrillMailAddress {
    private String email;

    @SuppressWarnings("unused")
    private MandrillMailAddress() {
    }

    public MandrillMailAddress(MailAddress mailAddress) {
        this.email = mailAddress.getMailAddress();
    }

    @SuppressWarnings("unused")
    public String getEmail() {
        return email;
    }

    @SuppressWarnings("unused")
    public void setEmail(String email) {
        this.email = email;
    }
}
