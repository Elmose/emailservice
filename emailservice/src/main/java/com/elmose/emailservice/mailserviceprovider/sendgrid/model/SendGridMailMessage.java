package com.elmose.emailservice.mailserviceprovider.sendgrid.model;

import com.elmose.emailservice.api.v1.model.MailMessage;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The model class represents a mail message when a request has been made to send a mail in relation to the SendGrid mail service API.
 */
@JsonPropertyOrder(alphabetic=true)
public class SendGridMailMessage {
    private List<SendGridPersonalization> personalizations;
    private SendGridMailAddress from;
    private List<SendGridContent> content;
    @JsonProperty("mail_settings")
    private SendGridMailSettings mailSettings;

    @SuppressWarnings("unused")
    private SendGridMailMessage() {
    }

    public SendGridMailMessage(MailMessage mailMessage) {
        personalizations = new ArrayList<>();
        personalizations.add(new SendGridPersonalization(mailMessage));
        from = new SendGridMailAddress(mailMessage);
        content = new ArrayList<>();
        content.add(new SendGridContent(mailMessage));
        mailSettings = new SendGridMailSettings(new SendGridSandboxMode());
    }

    @SuppressWarnings("unused")
    public List<SendGridPersonalization> getPersonalizations() {
        return personalizations;
    }

    @SuppressWarnings("unused")
    public void setPersonalizations(List<SendGridPersonalization> personalizations) {
        this.personalizations = personalizations;
    }

    @SuppressWarnings("unused")
    public SendGridMailAddress getFrom() {
        return from;
    }

    @SuppressWarnings("unused")
    public void setFrom(SendGridMailAddress from) {
        this.from = from;
    }

    @SuppressWarnings("unused")
    public List<SendGridContent> getContent() {
        return content;
    }

    @SuppressWarnings("unused")
    public SendGridMailSettings getMailSettings() {
        return mailSettings;
    }

    @SuppressWarnings("unused")
    public void setMailSettings(SendGridMailSettings mailSettings) {
        this.mailSettings = mailSettings;
    }

    @SuppressWarnings("unused")
    public void setContent(List<SendGridContent> content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendGridMailMessage that = (SendGridMailMessage) o;
        return Objects.equals(personalizations, that.personalizations) &&
                Objects.equals(from, that.from) &&
                Objects.equals(content, that.content);
    }
}
