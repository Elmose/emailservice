package com.elmose.emailservice.mailserviceprovider.sendgrid.api;

import com.elmose.emailservice.communication.request.PostHttpRequest;
import com.elmose.emailservice.communication.response.Response;
import com.elmose.emailservice.mailserviceprovider.ApiClient;
import com.elmose.emailservice.mailserviceprovider.sendgrid.SendGridConstants;
import com.elmose.emailservice.model.entity.MailServiceProvider;

import java.util.Map;

/**
 * The class contains all calls the the SendGrid mail service API
 */
public class SendGridClient implements ApiClient {
    private final MailServiceProvider mailServiceProvider;

    public SendGridClient(MailServiceProvider mailServiceProvider) {
        this.mailServiceProvider = mailServiceProvider;
    }

    public Response sendMail(Map<String, String> requestProperties, String message){
        return new PostHttpRequest(mailServiceProvider.getBaseUrl(), SendGridConstants.MAIL_SEND_SUBURL, requestProperties, message).execute();
    }
}
