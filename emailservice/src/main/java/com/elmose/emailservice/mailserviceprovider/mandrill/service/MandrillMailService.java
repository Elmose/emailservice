package com.elmose.emailservice.mailserviceprovider.mandrill.service;

import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.communication.response.Response;
import com.elmose.emailservice.mailserviceprovider.mandrill.api.MandrillClient;
import com.elmose.emailservice.mailserviceprovider.mandrill.model.MandrillMailMessage;
import com.elmose.emailservice.mailserviceprovider.mandrill.model.MandrillMailResponse;
import com.elmose.emailservice.mailserviceprovider.service.MailSenderService;
import com.elmose.emailservice.mapper.JsonObjectMapper;
import com.elmose.emailservice.model.MailServiceProviderConfiguration;
import com.elmose.emailservice.model.MailServiceProviderType;
import com.elmose.emailservice.model.entity.MailServiceProvider;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.logging.Logger;

/**
 * The service sends a mail to the Mandrill mail service provider via the {@link MandrillClient} and interprets the result as to whether a mail was successfully sent.
 */
public class MandrillMailService extends MailSenderService<MandrillClient> {
    private static final Logger LOGGER = Logger.getLogger(MandrillMailService.class.getName());
    private static final MailServiceProviderType MAIL_SERVICE_PROVIDER_TYPE = MailServiceProviderType.MANDRILL;

    public MandrillMailService(List<MailServiceProviderConfiguration> providerConfigurations) {
        super(providerConfigurations);
    }

    @Override
    protected MandrillClient createApiClient(MailServiceProvider mailServiceProvider) {
        return new MandrillClient(mailServiceProvider);
    }

    /**
     * @return The mail service provider type. This type is used to map to the mail service provider configuration made by the individual customer.
     */
    @Override
    protected MailServiceProviderType getMailServiceProviderType() {
        return MAIL_SERVICE_PROVIDER_TYPE;
    }

    MandrillMailService(MandrillClient client, MailServiceProviderConfiguration providerConfiguration) {
        super();
        this.client = client;
        this.mailServiceProviderConfiguration = providerConfiguration;
    }

    @Override
    public boolean sendMail(MailMessage mailMessage) {
        String message = getMessage(mailMessage, mailServiceProviderConfiguration.getCustomerProviderConfiguration().getDecodedApiKeyFromEncodedApiKey());
        Response response = client.sendMail(message);
        return isMailSent(response);
    }

    /**
     * Determines whether the mail was sent according to the business logic for the Mandrill API.
     * @param postHttpResponse The response from calling the API.
     * @return Whether the mail was successfully sent
     */
    private boolean isMailSent(Response postHttpResponse) {
        boolean mailSent = false;
        if (mailAttemptedSent(postHttpResponse)){
            List<MandrillMailResponse> mandrillMailResponse = new JsonObjectMapper<MandrillMailResponse>().toObjectListIgnoreUnknownProperties(postHttpResponse.getResponseText(), MandrillMailResponse.class);
            if (mandrillMailResponse.size() == 1 && mailSuccessfullySent(mandrillMailResponse)){
                mailSent = true;
            }else{
                LOGGER.warning("Reject reason on failed mail is: " + mandrillMailResponse.get(0).getRejectReason());
            }
        }
        return mailSent;
    }

    private boolean mailSuccessfullySent(List<MandrillMailResponse> mandrillMailResponse) {
        return mandrillMailResponse.get(0).getStatus().equals(MandrillMailResponse.MAIL_SENT_STATUS);
    }

    private boolean mailAttemptedSent(Response postHttpResponse) {
        return postHttpResponse.getHttpStatusCode() == HttpURLConnection.HTTP_OK;
    }

    private String getMessage(MailMessage mailMessage, String apiKey) {
        return new JsonObjectMapper<MandrillMailMessage>().toJsonString(new MandrillMailMessage(mailMessage, apiKey));
    }
}
