package com.elmose.emailservice.mailserviceprovider.sendgrid.model;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

/**
 * The model class represents a the sandbox mode of SendGrid, i.e. whether mails should actually be sent to mailboxes or only registered in SendGrid for test mode
 */
@JsonPropertyOrder(alphabetic=true)
public class SendGridSandboxMode {
    private boolean enable = true;

    public SendGridSandboxMode() {
    }

    @SuppressWarnings("unused")
    public boolean isEnable() {
        return enable;
    }

    @SuppressWarnings("unused")
    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
