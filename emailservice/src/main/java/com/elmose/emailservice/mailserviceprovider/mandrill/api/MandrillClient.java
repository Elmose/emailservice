package com.elmose.emailservice.mailserviceprovider.mandrill.api;

import com.elmose.emailservice.communication.request.PostHttpRequest;
import com.elmose.emailservice.communication.response.Response;
import com.elmose.emailservice.mailserviceprovider.ApiClient;
import com.elmose.emailservice.mailserviceprovider.mandrill.MandrillConstants;
import com.elmose.emailservice.model.entity.MailServiceProvider;

import java.util.logging.Logger;

/**
 * The class contains all calls the the Mandrill mail service API
 */
public class MandrillClient implements ApiClient{
    private static final Logger LOGGER = Logger.getLogger(MandrillClient.class.getName());
    private final MailServiceProvider mailServiceProvider;

    public MandrillClient(MailServiceProvider mailServiceProvider) {
        this.mailServiceProvider = mailServiceProvider;
    }

    public Response sendMail(String message){
        LOGGER.info("Sending mail " + message + " via Mandrill");
        return new PostHttpRequest(mailServiceProvider.getBaseUrl(), MandrillConstants.MAIL_SEND_SUBURL, message).execute();
    }
}
