package com.elmose.emailservice.mailserviceprovider.service;

import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.exception.mailsender.MissingMailServiceProviderConfigurationException;
import com.elmose.emailservice.mailserviceprovider.ApiClient;
import com.elmose.emailservice.model.MailServiceProviderConfiguration;
import com.elmose.emailservice.model.MailServiceProviderType;
import com.elmose.emailservice.model.entity.MailServiceProvider;

import java.util.List;

/**
 * An abstract class defining a mail service provider implementation.
 * For each new mail service provider a concrete implementation must be made.
 * @param <T> The type of the related API client for the mail service provider.
 */
public abstract class MailSenderService<T extends ApiClient> {
    protected MailServiceProviderConfiguration mailServiceProviderConfiguration;
    protected T client;

    public MailSenderService() {
    }

    /**
     *
     * @param providerConfigurations A set of configurations of which one must match the concrete mail service provider implementation
     */
    protected MailSenderService(List<MailServiceProviderConfiguration> providerConfigurations) {
        mailServiceProviderConfiguration = getMailServiceProviderConfiguration(providerConfigurations);
        client = createApiClient(mailServiceProviderConfiguration.getMailServiceProvider());
    }

    protected abstract T createApiClient(MailServiceProvider mailServiceProvider);

    protected abstract MailServiceProviderType getMailServiceProviderType();

    /**
     * Sends a mail to an external mail service provider and determines whether the mail was successfully sent.
     * @param mailMessage The mail message to send.
     * @return Whether the mail message was successfully sent.
     */
    public abstract boolean sendMail(MailMessage mailMessage);

    /**
     * @param providerConfigurations A set of possible configurations.
     * @return A mail service provider configuration which matches the current mail service provider implementation. The match is made by the type of the implementation and the configuration
     */
    private MailServiceProviderConfiguration getMailServiceProviderConfiguration(List<MailServiceProviderConfiguration> providerConfigurations) {
        for (MailServiceProviderConfiguration providerConfiguration : providerConfigurations) {
            if (getMailServiceProviderType() == providerConfiguration.getMailServiceProvider().getType()){
                return providerConfiguration;
            }
        }
        throw new MissingMailServiceProviderConfigurationException("Configuration for mail service provider of type " + getMailServiceProviderType() + " is missing");
    }

    public int getPriority() {
        return mailServiceProviderConfiguration.getCustomerProviderConfiguration().getPriority();
    }

    @Override
    public String toString() {
        return getMailServiceProviderType().name();
    }
}
