package com.elmose.emailservice.dao;

import com.elmose.emailservice.model.MailServiceProviderType;
import com.elmose.emailservice.model.entity.Customer;
import com.elmose.emailservice.model.entity.CustomerProviderConfiguration;
import com.elmose.emailservice.model.entity.MailServiceProvider;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.cache.AsyncCacheFilter;
import com.googlecode.objectify.util.Closeable;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CustomerProviderConfigurationDaoTest {
    private Closeable session;
    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
    private CustomerProviderConfigurationDao customerProviderConfigurationDao;

    @BeforeClass
    public static void setUpBeforeClass() {
        ObjectifyService.setFactory(new ObjectifyFactory());
        ObjectifyService.register(Customer.class);
        ObjectifyService.register(MailServiceProvider.class);
        ObjectifyService.register(CustomerProviderConfiguration.class);
    }

    @Before
    public void setUp() {
        this.session = ObjectifyService.begin();
        helper.setUp();
        customerProviderConfigurationDao = new CustomerProviderConfigurationDao();
    }

    @After
    public void tearDown() {
        AsyncCacheFilter.complete();
        this.session.close();
        helper.tearDown();
    }

    @Test
    public void whenCreatingACustomerProviderConfigurationThenTheCustomerProviderConfigurationIsPersisted() {
        //given
        Customer customer = new Customer();
        customer.setId(123L);
        customer.setName("Some customer name");
        customer.setEncodedApiKeyFromDecodedApiKey("Some API key");

        MailServiceProvider provider = new MailServiceProvider();
        provider.setId(456L);
        provider.setBaseUrl("https://someprovider.com/v1/");
        provider.setType(MailServiceProviderType.SEND_GRID);

        CustomerProviderConfiguration configuration = new CustomerProviderConfiguration();
        configuration.setCustomerKey(customer);
        configuration.setMailServiceProviderKey(provider);
        configuration.setEncodedApiKeyFromDecodedApiKey("someApiKey");
        configuration.setPriority(1);

        //when
        customerProviderConfigurationDao.createOrUpdate(configuration);

        //then
        List<CustomerProviderConfiguration> configurations = customerProviderConfigurationDao.getAll();
        assertEquals(1, configurations.size());
        CustomerProviderConfiguration fetchedConfiguration = configurations.get(0);
        assertNotNull(fetchedConfiguration.getId());
        assertEquals(customer.getId(), (Long)fetchedConfiguration.getCustomerKey().key().getId());
        assertEquals(provider.getId(), (Long)fetchedConfiguration.getMailServiceProviderKey().key().getId());
        assertEquals("someApiKey", fetchedConfiguration.getDecodedApiKeyFromEncodedApiKey());
        assertEquals(1, fetchedConfiguration.getPriority());
    }

    @Test
    public void whenCreatingACustomerProviderConfigurationThenItCanBeRetrievedByCustomerRef() {
        //given
        Customer customer = new Customer();
        customer.setId(123L);
        customer.setName("Some customer name");
        customer.setEncodedApiKeyFromDecodedApiKey("Some API key");

        MailServiceProvider provider = new MailServiceProvider();
        provider.setId(456L);
        provider.setBaseUrl("https://someprovider.com/v1/");
        provider.setType(MailServiceProviderType.SEND_GRID);

        CustomerProviderConfiguration configuration = new CustomerProviderConfiguration();
        configuration.setCustomerKey(customer);
        configuration.setMailServiceProviderKey(provider);
        configuration.setEncodedApiKeyFromDecodedApiKey("someApiKey");
        configuration.setPriority(1);

        //when
        customerProviderConfigurationDao.createOrUpdate(configuration);

        //then
        List<CustomerProviderConfiguration> configurations = customerProviderConfigurationDao.getByCustomerKey(Ref.create(customer));
        assertEquals(1, configurations.size());
        CustomerProviderConfiguration fetchedConfiguration = configurations.get(0);
        assertNotNull(fetchedConfiguration.getId());
        assertEquals(customer.getId(), (Long)fetchedConfiguration.getCustomerKey().key().getId());
        assertEquals(provider.getId(), (Long)fetchedConfiguration.getMailServiceProviderKey().key().getId());
        assertEquals("someApiKey", fetchedConfiguration.getDecodedApiKeyFromEncodedApiKey());
        assertEquals(1, fetchedConfiguration.getPriority());
    }
}