package com.elmose.emailservice.dao;

import com.elmose.emailservice.api.v1.ApiKeyHandler;
import com.elmose.emailservice.model.entity.Customer;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cache.AsyncCacheFilter;
import com.googlecode.objectify.util.Closeable;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CustomerDaoTest {
    private Closeable session;
    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
    private CustomerDao customerDao;

    @BeforeClass
    public static void setUpBeforeClass() {
        ObjectifyService.setFactory(new ObjectifyFactory());
        ObjectifyService.register(Customer.class);
    }

    @Before
    public void setUp() {
        this.session = ObjectifyService.begin();
        helper.setUp();
        customerDao = new CustomerDao();
    }

    @After
    public void tearDown() {
        AsyncCacheFilter.complete();
        this.session.close();
        helper.tearDown();
    }

    @Test
    public void whenCreatingACustomerThenTheCustomerIsPersisted() {
        //given
        Customer customer = new Customer();
        customer.setName("Some customer name");
        customer.setEncodedApiKeyFromDecodedApiKey("Some API key");

        //when
        customerDao.createOrUpdate(customer);

        //then
        List<Customer> customers = customerDao.getAll();
        assertEquals(1, customers.size());
        Customer fetchedCustomer = customers.get(0);
        assertNotNull(fetchedCustomer.getId());
        assertEquals("Some customer name", fetchedCustomer.getName());
        assertEquals("Some API key", fetchedCustomer.getDecodedApiKeyFromEncodedApiKey());
    }

    @Test
    public void whenCreatingACustomerThenTheCustomerCanBeRetrievedByApiKey() {
        //given
        Customer customer = new Customer();
        customer.setName("Some customer name");
        customer.setEncodedApiKeyFromDecodedApiKey("Some API key");

        //when
        customerDao.createOrUpdate(customer);

        //then
        assertNotNull(customerDao.getByEncodedApiKey(ApiKeyHandler.getEncodedApiKeyFromDecodedApiKey("Some API key")));
    }
}