package com.elmose.emailservice.dao;

import com.elmose.emailservice.model.MailServiceProviderType;
import com.elmose.emailservice.model.entity.MailServiceProvider;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cache.AsyncCacheFilter;
import com.googlecode.objectify.util.Closeable;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MailServiceProviderDaoTest {
    private Closeable session;
    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
    private MailServiceProviderDao mailServiceProviderDao;

    @BeforeClass
    public static void setUpBeforeClass() {
        ObjectifyService.setFactory(new ObjectifyFactory());
        ObjectifyService.register(MailServiceProvider.class);
    }

    @Before
    public void setUp() {
        this.session = ObjectifyService.begin();
        helper.setUp();
        mailServiceProviderDao = new MailServiceProviderDao();
    }

    @After
    public void tearDown() {
        AsyncCacheFilter.complete();
        this.session.close();
        helper.tearDown();
    }

    @Test
    public void whenCreatingAMailServiceProviderThenTheMailServiceProviderIsPersisted() {
        //given
        MailServiceProvider provider = new MailServiceProvider();
        provider.setBaseUrl("https://someprovider.com/v1/");
        provider.setType(MailServiceProviderType.SEND_GRID);

        //when
        mailServiceProviderDao.createOrUpdate(provider);

        //then
        List<MailServiceProvider> providers = mailServiceProviderDao.getAll();
        assertEquals(1, providers.size());
        MailServiceProvider fetchedProvider = providers.get(0);
        assertNotNull(fetchedProvider.getId());
        assertEquals("https://someprovider.com/v1/", fetchedProvider.getBaseUrl());
        assertEquals(MailServiceProviderType.SEND_GRID, fetchedProvider.getType());
    }
}