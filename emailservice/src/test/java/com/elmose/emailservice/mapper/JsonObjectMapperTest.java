package com.elmose.emailservice.mapper;

import com.elmose.emailservice.api.v1.model.MailAddress;
import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.mailserviceprovider.mandrill.model.MandrillMailMessage;
import com.elmose.emailservice.mailserviceprovider.mandrill.model.MandrillMailResponse;
import com.elmose.emailservice.mailserviceprovider.sendgrid.model.SendGridMailMessage;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class JsonObjectMapperTest {
    @Test
    public void whenConvertingToJsonStringThenResultIsValid(){
        //given
        JsonObjectMapper<SendGridMailMessage> jsonObjectMapper = new JsonObjectMapper<>();
        MailMessage mailMessage = new MailMessage(new MailAddress("from@test.com"), new MailAddress("to@test.com"), "Some subject", "Some content");

        //when
        String message = jsonObjectMapper.toJsonString(new SendGridMailMessage(mailMessage));

        //then
        assertEquals("{\"content\":[{\"type\":\"text/plain\",\"value\":\"Some content\"}],\"from\":{\"email\":\"from@test.com\"},\"mail_settings\":{\"sandbox_mode\":{\"enable\":true}},\"personalizations\":[{\"subject\":\"Some subject\",\"to\":[{\"email\":\"to@test.com\"}]}]}", message);
    }

    @Test
    public void whenConvertingRequestToJsonStringThenResultIsValid(){
        //given
        JsonObjectMapper<MandrillMailMessage> jsonObjectMapper = new JsonObjectMapper<>();
        MailMessage mailMessage = new MailMessage(new MailAddress("from@test.com"), new MailAddress("to@test.com"), "Some subject", "Some content");

        //when
        String jsonString = jsonObjectMapper.toJsonString(new MandrillMailMessage(mailMessage, "someApiKey"));

        //then
        assertEquals("{\"key\":\"someApiKey\",\"message\":{\"from_email\":\"from@test.com\",\"html\":\"Some content\",\"subject\":\"Some subject\",\"to\":[{\"email\":\"to@test.com\"}]}}", jsonString);
    }

    @Test
    public void whenConvertingResponseArrayToPojoThenResultIsValid(){
        //given
        JsonObjectMapper<MandrillMailResponse> jsonObjectMapper = new JsonObjectMapper<>();

        //when
        List<MandrillMailResponse> mandrillMailResponse = jsonObjectMapper.toObjectListIgnoreUnknownProperties("[{\"email\":\"louiseelmose@gmail.com\",\"status\":\"sent\",\"_id\":\"ed4b52338b0f488a84213ad41f5e4c0d\",\"reject_reason\":null}]", MandrillMailResponse.class);

        //then
        assertEquals("sent", mandrillMailResponse.get(0).getStatus());
    }
}