package com.elmose.emailservice.model.sendgrid;

import com.elmose.emailservice.api.v1.model.MailAddress;
import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.mailserviceprovider.sendgrid.model.SendGridMailMessage;
import com.elmose.emailservice.mapper.JsonObjectMapper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SendGridMailMandrillMessageTest {
    @Test
    public void whenMailMessageIsConvertedToJsonThenTheResultMatchesTheSendGridSyntax() {
        //given
        String expectedJson = "{\"content\":[{\"type\":\"text/plain\",\"value\":\"Some content\"}],\"from\":{\"email\":\"from@test.com\"},\"mail_settings\":{\"sandbox_mode\":{\"enable\":true}},\"personalizations\":[{\"subject\":\"Some subject\",\"to\":[{\"email\":\"to@test.com\"}]}]}";
        MailMessage mailMessage = new MailMessage(new MailAddress("from@test.com"), new MailAddress("to@test.com"), "Some subject", "Some content");
        SendGridMailMessage sendGridMailMessage = new SendGridMailMessage(mailMessage);

        //when
        String actualJson = new JsonObjectMapper<SendGridMailMessage>().toJsonString(sendGridMailMessage);

        //then
        assertEquals(expectedJson, actualJson);
    }
}