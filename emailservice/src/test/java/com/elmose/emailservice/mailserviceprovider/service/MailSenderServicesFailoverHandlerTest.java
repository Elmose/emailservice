package com.elmose.emailservice.mailserviceprovider.service;

import com.elmose.emailservice.api.v1.model.MailAddress;
import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.exception.mailsender.NoMoreMailSendersException;
import com.elmose.emailservice.mailserviceprovider.ApiClient;
import com.elmose.emailservice.model.MailServiceProviderType;
import com.elmose.emailservice.model.entity.MailServiceProvider;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class MailSenderServicesFailoverHandlerTest {
    private MailSenderServicesFailoverHandler mailSenderServicesFailoverHandler;
    private List<MailSenderService> allMailSendables;
    private MailMessage mailMessage;
    private SuccessfulMailService successfulMailSender;
    private ErroneousMailService erroneousMailSender;

    @Before
    public void init() {
        allMailSendables = new ArrayList<>();
        mailMessage =  new MailMessage(new MailAddress("from@test.com"), new MailAddress("to@test.com"), "Some subject", "Some content");
    }

    @Test
    public void whenSendingMailThenFirstMailSenderIsTheHighestPrioritized() {
        //given
        successfulMailSender = new SuccessfulMailService(1);
        erroneousMailSender = new ErroneousMailService(2);
        allMailSendables.add(successfulMailSender);
        allMailSendables.add(erroneousMailSender);

        //when
        mailSenderServicesFailoverHandler = new MailSenderServicesFailoverHandler(true, allMailSendables);

        //then
        assertTrue(mailSenderServicesFailoverHandler.getNextMailSender().isPresent());
        assertEquals(successfulMailSender, mailSenderServicesFailoverHandler.getNextMailSender().get());
    }

    @Test
    public void whenFirstMailSenderFailsThenNextMailSenderIsSetToTheNextMailSender() {
        //given
        successfulMailSender = new SuccessfulMailService(2);
        erroneousMailSender = new ErroneousMailService(1);
        allMailSendables.add(erroneousMailSender);
        allMailSendables.add(successfulMailSender);
        mailSenderServicesFailoverHandler = new MailSenderServicesFailoverHandler(true, allMailSendables);

        //when
        mailSenderServicesFailoverHandler.sendMail(mailMessage);

        //then
        assertTrue(mailSenderServicesFailoverHandler.getNextMailSender().isPresent());
        assertEquals(successfulMailSender, mailSenderServicesFailoverHandler.getNextMailSender().get());
    }

    @Test
    public void whenMailSenderSucceedsThenNextMailSenderIsNotChanged() {
        //given
        successfulMailSender = new SuccessfulMailService(1);
        erroneousMailSender = new ErroneousMailService(2);
        allMailSendables.add(successfulMailSender);
        allMailSendables.add(erroneousMailSender);
        mailSenderServicesFailoverHandler = new MailSenderServicesFailoverHandler(true, allMailSendables);

        //when
        mailSenderServicesFailoverHandler.sendMail(mailMessage);

        //then
        assertTrue(mailSenderServicesFailoverHandler.getNextMailSender().isPresent());
        assertEquals(successfulMailSender, mailSenderServicesFailoverHandler.getNextMailSender().get());
    }

    @Test(expected = NoMoreMailSendersException.class)
    public void whenThereAreNoNextMailSenderThenSendMailFails() {
        //given
        erroneousMailSender = new ErroneousMailService(1);
        allMailSendables.add(erroneousMailSender);
        mailSenderServicesFailoverHandler = new MailSenderServicesFailoverHandler(true, allMailSendables);

        //when
        mailSenderServicesFailoverHandler.sendMail(mailMessage);
        mailSenderServicesFailoverHandler.sendMail(mailMessage);
    }

    @Test
    public void whenMailHasNotBeenSentAndThereAreNoNextMailSenderThenNextMailSenderIsNotDefined() {
        //given
        erroneousMailSender = new ErroneousMailService(2);
        allMailSendables.add(erroneousMailSender);
        mailSenderServicesFailoverHandler = new MailSenderServicesFailoverHandler(true, allMailSendables);

        //when
        mailSenderServicesFailoverHandler.sendMail(mailMessage);

        //then
        assertFalse(mailSenderServicesFailoverHandler.getNextMailSender().isPresent());
    }

    @Test(expected = NoMoreMailSendersException.class)
    public void whenMailSendersAreNotDefinedThenSendMailFails() {
        //given
        mailSenderServicesFailoverHandler = new MailSenderServicesFailoverHandler(true, allMailSendables);

        //when
        mailSenderServicesFailoverHandler.sendMail(mailMessage);
    }

    @Test
    public void whenMailSendersAreConfiguredThenMailIsSentToHighestPrioritizedSender() {
        //given
        erroneousMailSender = new ErroneousMailService(100);
        successfulMailSender = new SuccessfulMailService(1);
        allMailSendables.add(erroneousMailSender);
        allMailSendables.add(successfulMailSender);
        mailSenderServicesFailoverHandler = new MailSenderServicesFailoverHandler(true, allMailSendables);

        //when
        mailSenderServicesFailoverHandler.sendMail(mailMessage);

        //then
        assertTrue(mailSenderServicesFailoverHandler.getNextMailSender().isPresent());
    }

    private class ErroneousMailService extends MailSenderService {
        private final int priority;

        public ErroneousMailService(int priority) {
            this.priority = priority;
        }

        @Override
        protected ApiClient createApiClient(MailServiceProvider mailServiceProvider) {
            return null;
        }

        @Override
        protected MailServiceProviderType getMailServiceProviderType() {
            return MailServiceProviderType.SEND_GRID;
        }

        @Override
        public boolean sendMail(MailMessage mailMessage) {
            return false;
        }

        @Override
        public int getPriority() {
            return priority;
        }
    }

    private class SuccessfulMailService extends MailSenderService {
        private final int priority;

        public SuccessfulMailService(int priority) {
            this.priority = priority;
        }

        @Override
        protected ApiClient createApiClient(MailServiceProvider mailServiceProvider) {
            return null;
        }

        @Override
        protected MailServiceProviderType getMailServiceProviderType() {
            return MailServiceProviderType.MANDRILL;
        }

        @Override
        public boolean sendMail(MailMessage mailMessage) {
            return true;
        }

        @Override
        public int getPriority() {
            return priority;
        }
    }
}