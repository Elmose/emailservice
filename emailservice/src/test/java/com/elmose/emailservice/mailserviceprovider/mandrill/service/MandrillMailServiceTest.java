package com.elmose.emailservice.mailserviceprovider.mandrill.service;

import com.elmose.emailservice.api.v1.model.MailAddress;
import com.elmose.emailservice.api.v1.model.MailMessage;
import com.elmose.emailservice.communication.response.Response;
import com.elmose.emailservice.mailserviceprovider.mandrill.api.MandrillClient;
import com.elmose.emailservice.model.MailServiceProviderConfiguration;
import com.elmose.emailservice.model.entity.CustomerProviderConfiguration;
import com.google.appengine.repackaged.org.apache.commons.httpclient.util.HttpURLConnection;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MandrillMailServiceTest {
    private MandrillMailService mailService;
    private MandrillClient client;

    @Before
    public void init(){
        client = mock(MandrillClient.class);
        MailServiceProviderConfiguration providerConfiguration = mock(MailServiceProviderConfiguration.class);
        when(providerConfiguration.getCustomerProviderConfiguration()).thenReturn(mock(CustomerProviderConfiguration.class));
        mailService = new MandrillMailService(client, providerConfiguration);
    }

    @Test
    public void whenHttpStatusCodeIsErroneounusThenMailIsNotSent(){
        //given
        MailMessage mailMessage = new MailMessage(new MailAddress("from@test.com"), new MailAddress("to@test.com"), "Some subject", "Some content");
        Response response = new Response("{\"email\":\"louiseelmose@gmail.com\",\"status\":\"sent\",\"_id\":\"ed4b52338b0f488a84213ad41f5e4c0d\",\"reject_reason\":null}", HttpURLConnection.HTTP_BAD_REQUEST);
        when(client.sendMail(any(String.class))).thenReturn(response);

        //when
        boolean mailSent = mailService.sendMail(mailMessage);

        //then
        assertFalse(mailSent);
    }

    @Test
    public void whenHttpStatusCodeIsHttpOkAndMailRejectedThenMailIsNotSent(){
        //given
        MailMessage mailMessage = new MailMessage(new MailAddress("from@test.com"), new MailAddress("to@test.com"), "Some subject", "Some content");
        Response response = new Response("[{\"email\":\"louiseelmose@gmail.com\",\"status\":\"failed\",\"_id\":\"ed4b52338b0f488a84213ad41f5e4c0d\",\"reject_reason\":\"Some reason\"}]", HttpURLConnection.HTTP_OK);
        when(client.sendMail(any(String.class))).thenReturn(response);

        //when
        boolean mailSent = mailService.sendMail(mailMessage);

        //then
        assertFalse(mailSent);
    }

    @Test
    public void whenHttpStatusCodeIsHttpOkAndMailAcceptedThenMailIsNotSent(){
        //given
        MailMessage mailMessage = new MailMessage(new MailAddress("from@test.com"), new MailAddress("to@test.com"), "Some subject", "Some content");
        Response response = new Response("[{\"email\":\"louiseelmose@gmail.com\",\"status\":\"sent\",\"_id\":\"ed4b52338b0f488a84213ad41f5e4c0d\",\"reject_reason\":null}]", HttpURLConnection.HTTP_OK);
        when(client.sendMail(any(String.class))).thenReturn(response);

        //when
        boolean mailSent = mailService.sendMail(mailMessage);

        //then
        assertTrue(mailSent);
    }
}